package br.com.senac.calculodesalario;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CalculadoraDeSalarioTest {

    /*
    As regras de negocio são as seguintes:
    Desenvolvedores possuem 20% de desconto caso seu salario
    seja maior do que R$ 3000,0. Caso contrario, o desconto e de 10%.
    DBAs e testadores possuem desconto de 25% se seus salários forem 
    maiores do que R$ 2500,0. 15%, em caso contrario.
    ANALISTA MAIS QUE 3000 15% E MENOS 5%
     */
    private Funcionario f;
    private CalculadoraDeSalario calculadoraDeSalario;

    @Before
    public void init() {

        f = new Funcionario();
        calculadoraDeSalario = new CalculadoraDeSalario();

    }

    @Test
    public void deveCalcularSalarioDesenvolvedorAbaixoLimite() {
        f.setSalario(1000);
        f.setCargo(Cargo.DESENVOLVEDOR);

        double salarioCalculado = calculadoraDeSalario.calcular(f);
        assertEquals(1000 * 0.9, salarioCalculado, 0.01);

    }

    @Test
    public void deveCalcularSalarioDesenvolvedorAcimaLimite() {
        f.setSalario(5000);
        f.setCargo(Cargo.DESENVOLVEDOR);

        double salarioCalculado = calculadoraDeSalario.calcular(f);
        assertEquals(5000 * 0.80, salarioCalculado, 0.001);
    }

    @Test
    public void deveCalcularSalarioDBAETestadorAbaixoLimite() {
        f.setSalario(1500);
        f.setCargo(Cargo.DBA);
        double salarioCalculado = calculadoraDeSalario.calcular(f);
        assertEquals(1500 * 0.85, salarioCalculado, 0.001);
    }

    @Test
    public void deveCalcularSalarioDBAETestadorAcimaDoLimite() {
        f.setSalario(3000);
        f.setCargo(Cargo.DBA);

        double salarioCalculado = calculadoraDeSalario.calcular(f);
        assertEquals(3000 * 0.75, salarioCalculado, 0.001);
    }

    @Test
    public void deveCalcularSalarioAnalisaAbaixoDoLimite() {
        
        double salario = 3500 ; 
        f.setSalario(salario);
        f.setCargo(Cargo.ANALISTA);

        double salarioCalculado = calculadoraDeSalario.calcular(f);
        assertEquals(salario * 0.90, salarioCalculado, 0.001);
    }

    @Test
    public void deveCalcularSalarioAnalisaAcimaDoLimite() {
        f.setSalario(4001);
        f.setCargo(Cargo.ANALISTA);

        double salarioCalculado = calculadoraDeSalario.calcular(f);
        assertEquals(4001 * 0.80, salarioCalculado, 0.001);
    }

    @Test(expected = RuntimeException.class)
    public void naoDeveCalcularSalarioParaCargoNaoImplementado() {
        f.setSalario(1500);
        f.setCargo(Cargo.SUPORTE);

        double salarioCalculado = calculadoraDeSalario.calcular(f);
        assertEquals(1500 * 0.95, salarioCalculado, 0.001);

    }

}
