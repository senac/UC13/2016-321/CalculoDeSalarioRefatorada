package br.com.senac.calculodesalario;

public class VinteCincoOuQuinzePorCento extends RegraPadrao {

    public VinteCincoOuQuinzePorCento(double limite) {
        super(limite);
    }

    @Override
    protected double getTaxaSuperior() {
        return 0.75;
    }

    @Override
    protected double getTaxaInferior() {
        return 0.85;
    }

}
