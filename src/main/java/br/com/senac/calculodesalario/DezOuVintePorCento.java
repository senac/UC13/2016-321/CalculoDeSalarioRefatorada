package br.com.senac.calculodesalario;

public class DezOuVintePorCento extends RegraPadrao {

    public DezOuVintePorCento(double limite) {
        super(limite);
    }

    
    
    @Override
    protected double getTaxaSuperior() {
        return 0.80 ; 
    }

    @Override
    protected double getTaxaInferior() {
       return  0.9 ; 
    }

   

}
