package br.com.senac.calculodesalario;

public interface RegraDeCalculo {

    double calcula(Funcionario f);
    
    
}
