package br.com.senac.calculodesalario;

public enum Cargo {

    DESENVOLVEDOR(new DezOuVintePorCento(3000)),
    DBA(new VinteCincoOuQuinzePorCento(2500)),
    TESTADOR(new VinteCincoOuQuinzePorCento(2500)),
    ANALISTA(new DezOuVintePorCento(4000)),
    SUPORTE(null);

    private final RegraDeCalculo regra;

    private Cargo(RegraDeCalculo regra) {
        this.regra = regra;

    }

    public RegraDeCalculo getRegra() {
        return this.regra;
    }

}
